---
# Display name
name: Fei Qi

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Assistant Professor of Bioinformatics

# Organizations/Affiliations
organizations:
- name: Huaqiao University
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: Institute of Genomics, Huaqiao University

interests:
- Bioinformatics
- Genomics
- Structural biology
- Evolution
- Deep learning

education:
  courses:
  - course: PhD in Bioinformatics
    institution: Technical University of Munich
    year: 2018
  - course: MMed in Microbiology and Biochemical Pharmacy
    institution: East China University of Science and Technology
    year: 2012
  - course: BEng in Bioengineering
    institution: East China University of Science and Technology
    year: 2009

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/qifei9
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=Gm4bs0UAAAAJ
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/qifei9
- icon: github
  icon_pack: fab
  link: https://github.com/qifei9
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0003-4578-0146
- icon: impactstory
  icon_pack: ai
  link: https://profiles.impactstory.org/u/0000-0003-4578-0146
- icon: researchgate
  icon_pack: ai
  link: https://www.researchgate.net/profile/Fei_Qi24
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "qifei9@gmail.com"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---

I currently work in the Institute of Genomics at Huaqiao University as an assistant professor. I am broadly interested in bioinformatics, genomics, structural biology, evolution and deep learning.

My recent researches focus on RNA structure, translation process and long non-coding RNA.
