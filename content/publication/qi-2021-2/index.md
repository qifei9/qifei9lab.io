---
title: "Proline Codon Pair Selection Determines Ribosome Pausing Strength and Translation Efficiency in BacteriaA"
date: 2021-05-17
#publishDate: 2021-03-25T06:26:02.739014Z
authors: ["Ralph Krafczyk<sup>#</sup>", "**Fei Qi**<sup>#</sup>", "Alina Sieber", "Judith Mehler", "Kirsten Jung", "Dmitrij Frishman\\*", "Jürgen Lassak\\*"]
publication_types: ["2"]
abstract: "The speed of mRNA translation depends in part on the amino acid to be incorporated into the nascent chain. Peptide bond formation is especially slow with proline and two adjacent prolines can even cause ribosome stalling. While previous studies focused on how the amino acid context of a Pro-Pro motif determines the stalling strength, we extend this question to the mRNA level. Bioinformatics analysis of the Escherichia coli genome revealed significantly differing codon usage between single and consecutive prolines. We therefore developed a luminescence reporter to detect ribosome pausing in living cells, enabling us to dissect the roles of codon choice and tRNA selection as well as to explain the genome scale observations. Specifically, we found a strong selective pressure against CCC/U-C, a sequon causing ribosomal frameshifting even under wild-type conditions. On the other hand, translation efficiency as positive evolutionary driving force led to an overrepresentation of CCG. This codon is not only translated the fastest, but the corresponding prolyl-tRNA reaches almost saturating levels. By contrast, CCA, for which the cognate prolyl-tRNA amounts are limiting, is used to regulate pausing strength. Thus, codon selection both in discrete positions but especially in proline codon pairs can tune protein copy numbers."
featured: true
publication: "*Communications Biology*"
url_pdf: "https://doi.org/10.1038/s42003-021-02115-z"
doi: "10.1038/s42003-021-02115-z"
# Featured image
# To use, place an image named `featured.jpg/png` in your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
# Set `preview_only` to `true` to just use the image for thumbnails.
image:
  placement: 1
  caption: ""
  focal_point: ""
  preview_only: false
profile: false  # Show author profile?
share: true  # Show social sharing links?
---
