---
title: "Genome Re-annotation, Novel Strong Promoter Discovery and Sequence Analysis of Pichia pastoris Based on RNA-Seq"
date: 2014-01-01
publishDate: 2020-02-06T06:26:02.743622Z
authors: ["**Fei Qi**", "Meng-Hao Cai", "Zhou Xiang-shan", "Yuan-xing Zhang"]
publication_types: ["2"]
abstract: "The transcriptome of Pichia pastoris was sequenced using RNA-Seq technique. Based on the sequencing data, the genome of Pichia pastoris was re-annotated, including 861 revised positions of the genome sequence, 553 re-annotated transcripts, 249 newly discovered transcripts and 83 alternative splices. In the analysis of gene expression profiling, 2 novel strang promoters, named P437 and P1431 respectively, were discovered. The highest transcription levels of the transcripts directed by these promoters are, respectively, 1.78 and 3.40 times of that of the AOX1 gene. Several binding sites of transcription factor were revealed in P437 and P1431 by sequence analysis."
featured: false
publication: "*Journal of East China University of Science and Technology*"
profile: false  # Show author profile?
---

