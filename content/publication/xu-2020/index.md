---
title: "A CRISPR/Cas13-based approach demonstrates biological relevance of vlinc class of long non-coding RNAs in anticancer drug response"
date: 2020-12-01
publishDate: 2020-02-06T06:26:02.745968Z
authors: ["Dongyang Xu", "Ye Cai", "Lu Tang", "Xueer Han", "Fan Gao", "Huifen Cao", "**Fei Qi**", "Philipp Kapranov"]
publication_types: ["2"]
abstract: "Long non-coding (lnc) RNAs represent a fascinating class of transcripts that remains highly controversial mainly due to ambiguity surrounding overall biological relevance of these RNAs. Multitude of reverse genetics studies showing functionality of lncRNAs are unfortunately based on assays that are either plagued by non-specific effects and/or cannot unambiguously assign observed phenotypes to the transcript per se. Here, we show application of the novel CRISPR/Cas13 RNA knockdown system that has superior specificity compared to other transcript-targeting knockdown methods like RNAi. We applied this method to a novel widespread subclass of nuclear lncRNAs — very long intergenic non-coding (vlinc) RNAs — in a high-throughput phenotypic assay based on survival challenge in response to anticancer drug treatments. We used multiple layers of controls including mismatch control for each targeting gRNA to ensure uncovering true phenotype-transcript relationships. We found evidence supporting importance for cellular survival for up to 60% of the tested protein-coding mRNAs and, importantly, 64% of vlincRNAs. Overall, this study demonstrates utility of CRISPR/Cas13 as a highly sensitive and specific tool for reverse genetics study of both protein-coding genes and lncRNAs. Furthermore, importantly, this approach provides evidence supporting biological significance of the latter transcripts in anticancer drug response."
featured: false
publication: "*Scientific Reports*"
url_pdf: "http://www.nature.com/articles/s41598-020-58104-5"
doi: "10.1038/s41598-020-58104-5"
profile: false  # Show author profile?
---

