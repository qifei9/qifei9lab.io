---
title: "Hovlinc is a recently evolved class of ribozyme found in human lncRNA"
date: 2021-03-22
#publishDate: 2021-03-25T06:26:02.739014Z
authors: ["Yue Chen<sup>#</sup>", "**Fei Qi**<sup>#</sup>\\*", "Fan Gao", "Huifen Cao", "Dongyang Xu", "Kourosh Salehi-Ashtiani", "Philipp Kapranov\\*"]
publication_types: ["2"]
abstract: "Although naturally occurring catalytic RNA molecules—ribozymes—have attracted a great deal of research interest, very few have been identified in humans. Here, we developed a genome-wide approach to discovering self-cleaving ribozymes and identified a naturally occurring ribozyme in humans. The secondary structure and biochemical properties of this ribozyme indicate that it belongs to an unidentified class of small, self-cleaving ribozymes. The sequence of the ribozyme exhibits a clear evolutionary path, from its appearance between ~130 and ~65 million years ago (Ma), to acquiring self-cleavage activity very recently, ~13–10 Ma, in the common ancestors of humans, chimpanzees and gorillas. The ribozyme appears to be functional in vivo and is embedded within a long noncoding RNA belonging to a class of very long intergenic noncoding RNAs. The presence of a catalytic RNA enzyme in lncRNA creates the possibility that these transcripts could function by carrying catalytic RNA domains."
featured: true
publication: "*Nature Chemical Biology*"
url_pdf: "https://doi.org/10.1038/s41589-021-00763-0"
doi: "10.1038/s41589-021-00763-0"
# Featured image
# To use, place an image named `featured.jpg/png` in your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
# Set `preview_only` to `true` to just use the image for thumbnails.
image:
  placement: 1
  caption: ""
  focal_point: ""
  preview_only: false
profile: false  # Show author profile?
share: true  # Show social sharing links?
summary: "Here, we developed a genome-wide approach to discovering self-cleaving ribozymes and identified a naturally occurring ribozyme in humans. The secondary structure and biochemical properties of this ribozyme indicate that it belongs to an unidentified class of small, self-cleaving ribozymes. The sequence of the ribozyme exhibits a clear evolutionary path, from its appearance between ~130 and ~65 million years ago (Ma), to acquiring self-cleavage activity very recently, ~13–10 Ma, in the common ancestors of humans, chimpanzees and gorillas."
---
