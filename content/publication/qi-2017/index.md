---
title: "Melting temperature highlights functionally important RNA structure and sequence elements in yeast mRNA coding regions"
date: 2017-06-01
publishDate: 2020-02-06T06:26:02.744660Z
authors: ["**Fei Qi**", "Dmitrij Frishman"]
publication_types: ["2"]
abstract: "Secondary structure elements in the coding regions of mRNAs play an important role in gene expression and regulation, but distinguishing functional from non-functional structures remains challenging. Here we investigate the dependence of sequence-structure relationships in the coding regions on temperature based on the recent PARTE data by Wan et al. Our main finding is that the regions with high and low thermostability (high Tm and low Tm regions) are under evolutionary pressure to preserve RNA secondary structure and primary sequence, respectively. Sequences of low Tm regions display a higher degree of evolutionary conservation compared to high Tm regions. Low Tm regions are under strong synonymous constraint, while high Tm regions are not. These findings imply that high Tm regions contain thermo-stable functionally important RNA structures, which impose relaxed evolutionary constraint on sequence as long as the base-pairing patterns remain intact. By contrast, low thermostability regions contain single-stranded functionally important conserved RNA sequence elements accessible for binding by other molecules. We also find that theoretically predicted structures of paralogous mRNA pairs become more similar with growing temperature, while experimentally measured structures tend to diverge, which implies that the melting pathways of RNA structures cannot be fully captured by current computational approaches."
featured: true
publication: "*Nucleic Acids Research*"
url_pdf: "https://academic.oup.com/nar/article/3062788/Melting http://www.ncbi.nlm.nih.gov/pubmed/28335026 https://academic.oup.com/nar/article-lookup/doi/10.1093/nar/gkx161 http://www.pubmedcentral.nih.gov/articlerender.fcgi?artid=PMC5449622"
doi: "10.1093/nar/gkx161"
# Featured image
# To use, place an image named `featured.jpg/png` in your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
# Set `preview_only` to `true` to just use the image for thumbnails.
image:
  placement: 1
  caption: ""
  focal_point: ""
  preview_only: false
profile: false  # Show author profile?
# Summary. An optional shortened abstract.
summary: "In the coding regions of mRNAs, high Tm regions contain thermo-stable functionally important RNA structures, which impose relaxed evolutionary constraint on sequence as long as the base-pairing patterns remain intact. By contrast, low thermostability regions contain single-stranded functionally important conserved RNA sequence elements accessible for binding by other molecules."
---

