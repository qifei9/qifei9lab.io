---
title: "Methanol-Independent Protein Expression by AOX1 Promoter with trans-Acting Elements Engineering and Glucose-Glycerol-Shift Induction in Pichia pastoris"
date: 2017-02-01
publishDate: 2020-02-06T06:26:02.741397Z
authors: ["Jinjia Wang", "Xiaolong Wang", "Lei Shi", "**Fei Qi**", "Ping Zhang", "Yuanxing Zhang", "Xiangshan Zhou", "Zhiwei Song", "Menghao Cai"]
publication_types: ["2"]
abstract: "The alcohol oxidase 1 promoter (PAOX1) of Pichia pastoris is commonly used for high level expression of recombinant proteins. While the safety risk of methanol and tough process control for methanol induction usually cause problems especially in large-scale fermentation. By testing the functions of trans-acting elements of PAOX1 and combinatorially engineering of them, we successfully constructed a methanol-free PAOX1 start-up strain, in which, three transcription repressors were identified and deleted and, one transcription activator were overexpressed. The strain expressed 77% GFP levels in glycerol compared to the wide-type in methanol. Then, insulin precursor (IP) was expressed, taking which as a model, we developed a novel glucose-glycerol-shift induced PAOX1 start-up for this methanol-free strain. A batch phase with glucose of 40 g/L followed by controlling residual glucose not lower than 20 g/L was compatible for supporting cell growth and suppressing PAOX1. Then, glycerol induction was started after glucose used up. Accordingly, an optimal bioprocess was further determined, generating a high IP production of 2.46 g/L in a 5-L bioreactor with dramatical decrease of oxygen consumption and heat evolution comparing with the wild-type in methanol. This mutant and bioprocess represent a safe and efficient alternative to the traditional glycerol-repressed/methanol-induced PAOX1 system."
featured: false
publication: "*Scientific Reports*"
url_pdf: "http://www.ncbi.nlm.nih.gov/pubmed/28150747 http://www.pubmedcentral.nih.gov/articlerender.fcgi?artid=PMC5288789"
doi: "10.1038/srep41850"
profile: false  # Show author profile?
---

