---
title: "Authentication of equine DNA from highly processed donkey-hide glue (Colla Corii Asini) using SINE element"
date: 2011-01-01
publishDate: 2020-02-06T06:26:02.742669Z
authors: ["Pin Lv", "Yingjie Zhao", "**Fei Qi**", "Xiangshan Zhou", "Jinhua You", "Yufeng Qin", "Yuanxing Zhang"]
publication_types: ["2"]
abstract: "In order to detect the DNA from highly processed medicinal donkey-hide glue Colla Corii Asini from adulterants blended or totally substituted with horse, cattle, and pig tissues, various species-specific PCR assays based on highly repetitive SINE elements were established. For horse and donkey tissues, a two-step PCR assay was adopted, including an initial detection of equine DNA based on the equine SINE ERE-1 and a further detection of horse DNA based on the horse specific satellite. The assay can detect the equine DNA and the horse DNA from binary solid glue mixtures spiked with 0.1% Colla Corii Asini and 1% horse-hide glue respectively. In addition, for bovine and porcine detection, the primer pairs were designed upon 1.711B bovine repeat and PRE-1 respectively. The developed methods can detect bovine and porcine DNA from binary solid glue mixtures spiked with 0.1% cattle-hide glue and 0.1% pig-hide glue respectively. The result shows that our developed PCR assays were useful, convenient and sensitive for Colla Corii Asini identification."
featured: false
publication: "*Journal of Food and Drug Analysis*"
tags: ["colla corii asini", "donkey-hide glue", "highly processed material", "molecular identification", "sine"]
doi: "10.1177/0969733015615172"
profile: false  # Show author profile?
---

