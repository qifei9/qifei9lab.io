---
title: "Transcriptome analysis of Δmig1Δmig2 mutant reveals their roles in methanol catabolism, peroxisome biogenesis and autophagy in methylotrophic yeast Pichia pastoris"
date: 2018-12-01
publishDate: 2020-02-06T06:26:02.740264Z
authors: ["Lei Shi", "Xiaolong Wang", "Jinjia Wang", "Ping Zhang", "**Fei Qi**", "Menghao Cai", "Yuanxing Zhang", "Xiangshan Zhou"]
publication_types: ["2"]
abstract: "Two catabolite repressor genes (MIG1 and MIG2) were previously identified in Pichia pastoris, and the derepression of alcohol oxidase (AOX) expression was realized in Δmig1 or Δmig1Δmig2 mutants grown in glycerol, but not in glucose. In this study, genome-wide RNA-seq analysis of Δmig1Δmig2 and the wild-type strain grown in glycerol revealed that the expression of numerous genes was greatly altered. Nearly 7% (357 genes) of approximately 5276 genes annotated in P. pastoris were significantly upregulated, with at least a twofold differential expression in Δmig1Δmig2; the genes were mainly related to cell metabolism. Approximately 23% (1197 genes) were significantly downregulated; these were mainly correlated with the physiological characteristics of the cell. The methanol catabolism and peroxisome biogenesis pathways were remarkably enhanced, and the genes AOX1 and AOX2 were upregulated higher than 30-fold, which was consistent with the experimental results of AOX expression. The Mig proteins had a slight effect on autophagy when cells were grown in glycerol. The expression analysis of transcription factors showed that deletion of MIG1 and MIG2 significantly upregulated the binding of an essential transcription activator, Mit1p, with the AOX1 promoter, which suggested that Mig proteins might regulate the AOX1 promoter through the regulation of Mit1p. This work provides a reference for the further exploration of the methanol induction and catabolite repression mechanisms of AOX expression in methylotrophic yeasts."
featured: false
publication: "*Genes and Genomics*"
tags: ["alcohol oxidase", "catabolite repression", "mig", "pichia pastoris", "rna-seq"]
url_pdf: "http://link.springer.com/10.1007/s13258-017-0641-5"
doi: "10.1007/s13258-017-0641-5"
profile: false  # Show author profile?
---

