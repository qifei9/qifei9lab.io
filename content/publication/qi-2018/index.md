---
title: "Evolutionary analysis of polyproline motifs in Escherichia coli reveals their regulatory role in translation"
date: 2018-02-01
publishDate: 2020-02-06T06:26:02.739014Z
authors: ["**Fei Qi**", "Magdalena Motz", "Kirsten Jung", "Jürgen Lassak", "Dmitrij Frishman"]
publication_types: ["2"]
abstract: "Translation of consecutive prolines causes ribosome stalling, which is alleviated but cannot be fully compensated by the elongation factor P. However, the presence of polyproline motifs in about one third of the E. coli proteins underlines their potential functional importance, which remains largely unexplored. We conducted an evolutionary analysis of polyproline motifs in the proteomes of 43 E. coli strains and found evidence of evolutionary selection against translational stalling, which is especially pronounced in proteins with high translational efficiency. Against the overall trend of polyproline motif loss in evolution, we observed their enrichment in the vicinity of translational start sites, in the inter-domain regions of multi-domain proteins, and downstream of transmembrane helices. Our analysis demonstrates that the time gain caused by ribosome pausing at polyproline motifs might be advantageous in protein regions bracketing domains and transmembrane helices. Polyproline motifs might therefore be crucial for co-translational folding and membrane insertion."
featured: true
publication: "*PLoS Computational Biology*"
url_pdf: "http://dx.plos.org/10.1371/journal.pcbi.1005987"
doi: "10.1371/journal.pcbi.1005987"
# Featured image
# To use, place an image named `featured.jpg/png` in your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
# Set `preview_only` to `true` to just use the image for thumbnails.
image:
  placement: 1
  caption: ""
  focal_point: ""
  preview_only: false
profile: false  # Show author profile?
share: true  # Show social sharing links?
summary: "Translation of consecutive prolines causes ribosome stalling, and thus they are under nagative evolutionary selection which is especially pronounced in proteins with high translational efficiency. However, the time gain caused by ribosome pausing at polyproline motifs might be advantageous in protein regions bracketing domains and transmembrane helices. Polyproline motifs might therefore be crucial for co-translational folding and membrane insertion."
---
